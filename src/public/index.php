<?php

declare(strict_types = 1);
// require_once '../app/payment/Transaction.php';

spl_autoload_register(function($class) {
    $path = __DIR__ . '/../' . lcfirst(str_replace('\\','/',$class)) . '.php';

    if(file_exists($path)) {
        require $path;
    }
});

use App\Payment\Transaction;
$transaction = new Transaction(10,'transaction 1');
var_dump($transaction);

