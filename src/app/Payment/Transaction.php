<?php

declare(strict_types=1);

namespace App\Payment;
use DateTime as Banana;


class Transaction
{
    private float $amount;        

    public function __construct(float $amount,private string $description)
    {
        $this->amount = $amount;        
    }

    public function addTax(float $rate) : Transaction
    {
        $this->amount += $this->amount * $rate / 100;
        return $this;
    }

    public function applyDiscount(float $rate)  : Transaction
    {
        $this->amount -= $this->amount * $rate / 100;
        return $this;
    }

    public function getAmount() {
        var_dump(new Banana());
        return $this->amount;
    }

    public function getDescription() {
        return $this->description;
    }
}
